// 2022 - 2023
const taxBrackets = [
  {threshold: 18200,     flatRate: 0,      varRate: 0},
  {threshold: 45000,     flatRate: 0,      varRate: 0.19},
  {threshold: 120000,    flatRate: 5092,   varRate: 0.325},
  {threshold: 180000,    flatRate: 29467,  varRate: 0.37},
  {threshold: 999999999, flatRate: 51668,  varRate: 0.45}
];

// Check for tax bracket, and return the object index value
function taxCalc() {
  console.log(`Getting tax rates...`);
  for (let i = 0; i <= taxBrackets.length; i++) {
    if ( income <= taxBrackets[i].threshold ) {
      console.log(`Tax rates calculated!`);
      return i;
    }
  }
}

// Tax calculations
let taxBracket = taxCalc();
let dollarDifference = income - taxBrackets[taxBracket - 1].threshold;
let tax = ( (dollarDifference * taxBrackets[taxBracket].varRate) + taxBrackets[taxBracket].flatRate );