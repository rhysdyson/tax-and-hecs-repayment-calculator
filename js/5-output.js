document.querySelector('#htmlOutput').innerHTML += `
  <li>Taxable income: $${ income.toLocaleString("en-US") }</li>
  <li>Tax: $${ tax.toLocaleString("en-US") }</li>
  <li>Super: $${ superContribution.toLocaleString("en-US") }</li>
`;

document.querySelector('#htmlOutput').innerHTML += `
  <li>
    <h3>HECS Help Loan</h3>
      <ul>
        <li>
          Mandatory Repayment: $${ repayment.toLocaleString("en-US") }
        </li>
        <li>
          Indexation: $${ indexation.toLocaleString("en-US") } (${ indexationRate.toLocaleString("en-US") }%)
        </li>
        <li>
          Remaining Balance: $${ balance.toLocaleString("en-US")} <br> (Increase: $${(balance - loan).toLocaleString("en-US") })
        </li>
      </ul>
    </li>
`;

document.querySelector('#htmlOutput').innerHTML += `
  <li>Net income: $${ netIncome.toLocaleString("en-US") }</li>
  <li>Weekly Pay: $${ (netIncome/52).toLocaleString("en-US") }</li>
`;