// 2022 - 2023
let indexationRate = 3.9;

const hecsBrackets = [
  {threshold: 48361,  rate: 0},
  {threshold: 55836,  rate: 1.0},
  {threshold: 59186,  rate: 2.0},
  {threshold: 62738,  rate: 2.5},
  {threshold: 66502,  rate: 3.0},
  {threshold: 70492,  rate: 3.5},
  {threshold: 74722,  rate: 4.0},
  {threshold: 79206,  rate: 4.5},
  {threshold: 83958,  rate: 5.0},
  {threshold: 88996,  rate: 5.5},
  {threshold: 94336,  rate: 6.0},
  {threshold: 99996,  rate: 6.5},
  {threshold: 105996, rate: 7.0},
  {threshold: 112355, rate: 7.5},
  {threshold: 119097, rate: 8.0},
  {threshold: 126243, rate: 8.5},
  {threshold: 133818, rate: 9.0},
  {threshold: 141847, rate: 9.5},
  {threshold: 141848, rate: 10.0}
];

// Check for hecs bracket, and return the object index value
function hecsCalc() {
  for (let i = 0; i <= hecsBrackets.length; i++) {
    if ( income <= hecsBrackets[i].threshold ) {
      return i;
    }
  }
}

// HECS calculations
let hecsBracket = hecsCalc();
let repayment = income * (hecsBrackets[hecsBracket].rate / 100);
let indexation = loan * (indexationRate / 100);
let balance = loan + indexation - repayment;